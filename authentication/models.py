from django.db import models

# Create your models here.


class Credentials(models.Model):
    username = models.TextField(primary_key=True, blank=False)
    password = models.TextField(blank=False)
