from django import forms


class AuthForm(forms.Form):
    input_username = forms.CharField(label='User', required=True)
    input_pwd = forms.CharField(label='Password', widget=forms.PasswordInput())
