from django.shortcuts import render
from authentication.forms import AuthForm
from authentication.models import Credentials

# Create your views here.


def authentication(request):
    form = AuthForm()
    if request.POST:
        form = AuthForm(request.POST)
        if form.is_valid():
            input_username = form.cleaned_data['input_username']
            input_password = form.cleaned_data['input_pwd']
            queryset = Credentials.objects.filter(username__exact=input_username)
            if queryset.__len__() == 1:
                selected_user = queryset[0]
            else:
                print('user not exists!')
                return render(request, 'auth.html', {'form': form,
                                                     'error_msg': 'ERROR: Bad credentials!'})
            if input_username == selected_user.username and input_password == selected_user.password:
                return render(request, 'success.html')
            else:
                print('bad password')
                return render(request, 'auth.html', {'form': form,
                                                     'error_msg': 'ERROR: Bad credentials!'})
        else:
            return render(request, 'auth.html', {'form': form})
    else:
        return render(request, 'auth.html', {'form': form})
